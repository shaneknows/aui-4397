'use strict';

import globalize from './internal/globalize';

function escapeHtml (str) {
    return str.replace(/[&"'<>`]/g, function (str) {
        var special = {
            '<': '&lt;',
            '>': '&gt;',
            '&': '&amp;',
            '\'': '&#39;',
            '`': '&#96;'
        };

        if (typeof special[str] === 'string') {
            return special[str];
        }

        return '&quot;';
    });
}

globalize('escapeHtml', escapeHtml);

export default escapeHtml;
