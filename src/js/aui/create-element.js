'use strict';

import $ from './jquery';

function createElement () {
    var res = null;

    if (arguments.length && typeof arguments[0] === 'string') {
        res = $(document.createElement(arguments[0]));

        if (arguments.length === 2) {
            res.html(arguments[1]);
        }
    }

    const {DEBUG} = window;

    if (DEBUG) {
        //We can't use the deprecate module or we will introduce a circular dependency
        if (typeof console !== 'undefined' && console.warn) {
            console.warn(`AJS's create element functionality has been deprecated since 5.9.0.
No alternative will be provided.
Use document.createElement() or jQuery.parseHTML(), or preferably use a templating library.`);
        }
    }

    return res;
}

export default createElement;
