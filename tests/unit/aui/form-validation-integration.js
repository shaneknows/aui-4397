'use strict';

import '../../../src/js/aui/select';
import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';
import keyCode from '../../../src/js/aui/key-code';
import skate from '../../../src/js/aui/internal/skate';
import validator from '../../../src/js/aui/form-validation';

describe('aui/form-validation-integration', function () {
    describe('Form validation component integration', function () {
        var clock;

        afterEach(function () {
            $('.tipsy').remove();
        });

        function pressKey (keyCode) {
            helpers.pressKey(keyCode);
        }

        function fieldValidationState (el) {
            return $(el).attr('data-aui-validation-state');
        }

        describe('with single select', function () {
            var singleSelect;

            beforeEach(function () {
                clock = sinon.useFakeTimers();
                singleSelect = createAndSkate(
                        '<aui-select data-aui-validation-field data-aui-validation-required="required">' +
                        '<aui-option>Option 1</aui-option>' +
                        '<aui-option>Option 2</aui-option>' +
                        '<aui-option>Option 3</aui-option>' +
                        '</aui-select>'
                );
            });

            afterEach(function () {
                clock.restore();
                helpers.removeLayers();
            });

            function focusSingleSelectInput () {
                singleSelect._input.focus();
            }

            function createAndSkate (html) {
                var fixtures = helpers.fixtures({
                    html: html
                });

                return skate.init(fixtures.html);
            }

            it('validates successfully with the required validator', function () {
                focusSingleSelectInput();
                helpers.fakeTypingOut('Option 1');
                pressKey(keyCode.ENTER);
                validator.validate(singleSelect);
                fieldValidationState(singleSelect).should.equal('valid');
            });

            it('invalidates successfully with the required validator', function () {
                validator.validate(singleSelect);
                fieldValidationState(singleSelect).should.equal('invalid');
            });
        });
    });
});
