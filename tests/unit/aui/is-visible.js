'use strict';

import isVisible from '../../../src/js/aui/is-visible';

describe('aui/is-visible', function () {
    it('globals', function () {
        expect(AJS.isVisible).to.equal(isVisible);
    });
});
