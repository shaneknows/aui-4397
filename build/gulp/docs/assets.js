var gulp = require('gulp');
var rootPaths = require('../../lib/root-paths');

module.exports = function docsAssets () {
    return gulp.src(rootPaths('docs/**'))
        .pipe(gulp.dest('.tmp/docs'));
};
