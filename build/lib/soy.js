'use strict';

var sh = require('shelljs');

function toKeyVal (obj) {
    var parts = [];

    for (var a in obj) {
        if (obj.hasOwnProperty(a)) {
            parts.push(a + ':' + obj[a]);
        }
    }

    return parts.join(',');
}

function translateOption (opt) {
    if (typeof opt === 'object') {
        return toKeyVal(opt);
    }

    return opt;
}

var defaults = {
    jar: __dirname + '/../jar/atlassian-soy-cli-support-3.2.0-SOY-40-cli-support-1-jar-with-dependencies.jar',
    args: {
        basedir: undefined,
        data: undefined,
        dependencies: undefined,
        glob: undefined,
        i18n: undefined,
        outdir: undefined,
        rootnamespace: undefined,
        type: 'js'
    }
};

module.exports = function (opts) {
    opts = opts || {};
    var a;
    var parts = [
        process.env.JAVA_HOME ? process.env.JAVA_HOME + '/bin/java' : 'java',
        '-jar ' + (opts.jar || defaults.jar)
    ];

    for (a in defaults.args) {
        if (defaults.args.hasOwnProperty(a)) {
            var value = opts.args && opts.args[a] || defaults.args[a];

            if (value !== undefined) {
                parts.push('--' + a + ' ' + translateOption(value));
            }
        }
    }

    sh.exec(parts.join(' '));
};
